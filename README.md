# s3 pipeline

<!-- BADGIE TIME -->

[![pipeline status](https://img.shields.io/gitlab/pipeline-status/buildgarden/pipelines/s3?branch=main)](https://gitlab.com/buildgarden/pipelines/s3/-/commits/main)
[![latest release](https://img.shields.io/gitlab/v/release/buildgarden/pipelines/s3)](https://gitlab.com/buildgarden/pipelines/s3/-/releases)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

<!-- END BADGIE TIME -->

Publish static assets to an S3 bucket.

> Do not use this software unless you are an active collaborator on the
> associated research project.
>
> This project is an output of an ongoing, active research project. It is
> published without warranty, is subject to change at any time, and has not been
> certified, tested, assessed, or otherwise assured of safety by any person or
> organization. Use at your own risk.

The following objects are published to S3 via this pipeline:

- <https://pub-402d22bf1511450d8cb22c48819c273d.r2.dev/pipelines/s3/0.1.0/hello-world.txt>

- <https://pub-402d22bf1511450d8cb22c48819c273d.r2.dev/pipelines/s3/latest/hello-world.txt>

- <https://pub-402d22bf1511450d8cb22c48819c273d.r2.dev/pipelines/s3/stable/hello-world.txt>

## Configuration

This pipeline requires access to a provisioned S3 bucket. At a minimum, the
following configuration is required to connect to the bucket:

- **`S3_ACCESS_KEY`** - IAM access key.

- **`S3_SECRET_KEY`** - IAM secret key.

- **`S3_HOSTNAME`** - S3 endpoint URL.

- **`S3_BUCKET`** - Name of S3 bucket.

## Usage

To publish the latest commit on your default branch to a `latest` version:

```yaml
# .gitlab-ci.yml
include:
  - project: buildgarden/pipelines/s3
    file:
      - s3-sync-latest.yml
```

To publish the latest tagged release to a `stable` version:

```yaml
# .gitlab-ci.yml
include:
  - project: buildgarden/pipelines/s3
    file:
      - s3-sync-stable.yml
```

To publish the current tag as a standalone release:

```yaml
# .gitlab-ci.yml
include:
  - project: buildgarden/pipelines/s3
    file:
      - s3-sync-tag.yml
```

To do something else:

```yaml
# .gitlab-ci.yml
include:
  - project: buildgarden/pipelines/s3
    file:
      - s3-sync.yml
```
