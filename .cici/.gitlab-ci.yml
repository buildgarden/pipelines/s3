# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: MIT

stages:
  - test
  - build
  - deploy

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "push" && $CI_OPEN_MERGE_REQUESTS
      when: never
    - when: always

variables:
  S3_ALIAS: "s3"
  S3_HOSTNAME: ""
  S3_BUCKET: ""
  S3_ACCESS_KEY: ""
  S3_SECRET_KEY: ""
  S3_SOURCE: "${CI_PROJECT_DIR}/dist"
  S3_TARGET_PREFIX: ""
  S3_TARGET_PATH: ""
  S3_TARGET_VERSION: "${CI_COMMIT_REF_NAME}"
  S3_SYNC_OPTS: ""

s3-sync:
  stage: deploy
  image: "${CONTAINER_PROXY}alpine:3"
  script:
    # install mc
    - apk add --no-cache curl
    - curl -o /tmp/mc https://dl.min.io/client/mc/release/linux-amd64/mc
    - install /tmp/mc /usr/local/bin/mc
    - rm /tmp/mc
    - mc --version

    # prepare S3 connection
    - mc alias set "$S3_ALIAS" "$S3_HOSTNAME" "$S3_ACCESS_KEY" "$S3_SECRET_KEY"

    # sync contents
    - |-
      if [[ -z "$S3_TARGET_PATH" ]] ; then
        export S3_TARGET_PATH="$(echo "$CI_PROJECT_PATH" | sed -e 's@^'${CI_PROJECT_ROOT_NAMESPACE}'/@@')"
      fi
    - |-
      if [[ -z "$S3_TARGET_PREFIX" ]] ; then
        export S3_TARGET_PREFIX="${S3_TARGET_PATH}/${S3_TARGET_VERSION}"
      fi
    - export S3_TARGET="${S3_ALIAS}/${S3_BUCKET}/${S3_TARGET_PREFIX}"
    - mc mirror "$S3_SOURCE" "$S3_TARGET" $S3_SYNC_OPTS

s3-sync-latest:
  extends: s3-sync
  variables:
    S3_SYNC_OPTS: --overwrite
    S3_TARGET_VERSION: latest
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

s3-sync-stable:
  extends: s3-sync
  variables:
    S3_SYNC_OPTS: --overwrite
    S3_TARGET_VERSION: stable
  rules:
    - if: $CI_COMMIT_TAG

s3-sync-tag:
  extends: s3-sync
  rules:
    - if: $CI_COMMIT_TAG
